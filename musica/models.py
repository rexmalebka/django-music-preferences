from django.db import models
from django.forms import ModelForm, PasswordInput


class User(models.Model):
    nickname = models.CharField(max_length=100)
    password = models.CharField(max_length=10)


class Band(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    year_creation = models.IntegerField(default=2019)
    genre = models.CharField(max_length=50)


class Album(models.Model):
    band = models.ForeignKey(Band, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    year = models.IntegerField(default=2019)
    genre = models.CharField(max_length=50)


class Song(models.Model):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    duration = models.TimeField()


class SignForm(ModelForm):
    class Meta:
        model = User
        fields = ('nickname', 'password')
        widgets = {
            'password': PasswordInput()
        }


