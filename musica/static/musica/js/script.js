// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$("#loginForm").on("submit", function (e) {
	e.preventDefault();
	const csrftoken = getCookie('csrftoken');
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {

			if (xhttp.getResponseHeader('content-type') == "application/json"){
				let resp = JSON.parse(xhttp.responseText);
				if (resp["logged"]){
					//login succesful
					window.location = "/board";
				}else{
					$("#loginFormError").removeAttr("hidden");
					if(resp["error"]=="wrong creds"){
						$("#loginFormError").text("");
						$("#loginFormError").text("wrong nickname/password");
					}
					else if(resp["error"]=="user does not exists"){
						$("#loginFormError").text("");
						$("#loginFormError").append("User does not exists, want to <a href=\"/signup\">signup</a>?");
					}
				}
				console.log(resp);
			
			}
		}
  };

  xhttp.open("POST","/login/", true);
  xhttp.setRequestHeader("X-CSRFToken", csrftoken);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send( $("#loginForm").serialize());

});


$("#signupForm").on("submit", function (e) {
	e.preventDefault();
	const csrftoken = getCookie('csrftoken');
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {

			if (xhttp.getResponseHeader('content-type') == "application/json"){
				let resp = JSON.parse(xhttp.responseText);
				console.log(resp);
				if (resp['signedup']){
					$("#signupFormError").removeAttr("hidden");
					$("#signupFormError").text("");
					$("#signupFormError").append("User created, redirecting");				
					setTimeout(function(){window.location="/login"}, 1000);
				}else{
					$("#signupFormError").removeAttr("hidden");
					$("#signupFormError").text("");
					$("#signupFormError").append("User exists, try another one");				
				}
			}
		}
  };

  xhttp.open("POST","/signup/", true);
  xhttp.setRequestHeader("X-CSRFToken", csrftoken);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send( $("#signupForm").serialize());
});
