from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import loader
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from django.core.cache import cache
from . import models


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def index(request):
    return HttpResponseRedirect("/board")


def user(request, user):
    template = loader.get_template("board.html")
    usr = models.User.objects.get(nickname=user)
    bands = models.Band.objects.filter(user=usr.id)
    print(bands)
    return HttpResponse(
        template.render(
            context={'bands': bands, 'nickname': usr.nickname}), request)


def board(request):
    template = loader.get_template("board.html")

    if cache.get(get_client_ip(request), "0") == "0":
        # if session it's not initialized
        return HttpResponseRedirect("/login")
    else:
        # logged
        usr = models.User.objects.get(
            nickname=cache.get(get_client_ip(request), "0"))
        bands = models.Band.objects.filter(user=usr.id)
        return HttpResponse(
            template.render(
                context={'bands': bands, 'nickname': usr.nickname}),
            request)


def band(request, user, band):
    template = loader.get_template("band.html")
    usr = models.User.objects.get(nickname=user)
    bnd = models.Band.objects.filter(user=usr.id, name=band)[0]
    albums = models.Album.objects.filter(band = bnd)
    print(albums)
    return HttpResponse(
        template.render(
            context={ 'nickname': usr.nickname, 'band': bnd.name, 'albums': albums}), request)



def album(request, user, band, album):
    template = loader.get_template("album.html")
    usr = models.User.objects.get(nickname=user)
    bnd = models.Band.objects.filter(user=usr.id, name=band)[0]
    albm = models.Album.objects.filter(band = bnd, name=album)[0]
    songs = models.Song.objects.filter(album = albm)
    return HttpResponse(
        template.render(
            context={ 'nickname': usr.nickname, 'band': bnd.name, 'album': albm.name, 'songs':songs}), request)


@csrf_protect
@ensure_csrf_cookie
def login(request):
    LoginForm = models.SignForm()
    template = loader.get_template("login.html")

    print(cache.get(get_client_ip(request), "0"), 'cachee')
    if cache.get(get_client_ip(request), "0") == "0":
        # check if session is initialized
        if request.method == "POST":

            if models.User.objects.filter(
                nickname=request.POST['nickname']
            ).count() == 1:
                # User exists

                user = models.User.objects.get(nickname=request.POST['nickname'])
                if request.POST['password']==user.password:
                    # make session inside cache
                    cache.set(get_client_ip(request), request.POST['nickname'], 5*60)

                    return JsonResponse({"logged":True})
                else:
                    #bad passwrods
                    return JsonResponse({"logged":False,"error":"wrong creds"})
            else:
                # user does not exists, wont to create it ? yos / nah
                print("want to create user")
                return JsonResponse({"logged":False,"error":"user does not exists"})

        else:
            return HttpResponse(
                template.render(
                    context={'logged': False, 'loginform': LoginForm}),
                request)
    else:
        # session is initialized
        return HttpResponseRedirect("/board")


@csrf_protect
@ensure_csrf_cookie
def signup(request):
    SignUpForm = models.SignForm()
    template = loader.get_template("signup.html")

    if request.method == "POST":
        if models.User.objects.filter(nickname=request.POST['nickname']).count() == 1:
            return JsonResponse({"signedup": False,
                                 "error": "nickname exists"})
        else:
            # create user

            models.User.objects.create(nickname=request.POST['nickname'],
                                       password=request.POST['password'])
            return JsonResponse({"signedup": True})

    return HttpResponse(
        template.render(
            context={'logged': False, 'signupform': SignUpForm}),
        request)
