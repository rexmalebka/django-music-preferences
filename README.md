# project for django learning

## webpage for music preferences

a webpage for making personal pages of music preferences, albums and songs. A user can sign it, sign up, add and delete albums and songs

For this web server I used `django 2.1.5`, `SQLite` db, `memcached` for sessions, `nginx` for static files.

For the views I'm using the default templates, `bootstrap` for stylish, `jquery` for DOM manipulation.

I'm also using `flake8` for pep compatibility.

## Models

User:

- nickname (100 length max.)
- password (10 length max.)

Band:

- user (foreign key)
- name (100 length max.)
- year\_creation 
- genre

Album:

- band (foreign key)
- name
- year
- genre

Song:

- album (foreign key)
- name
- duration (time field)

## customized pages

to see the bands's user preferences:

- localhost:8080/user/_user_

to see the album's band preferences:

- localhost:8080/user/_user_/_band_/

to see the songs's album preferences:

- localhost:8080/user/_user_/_band_/_album_

